package main

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"golang-lab/viper/utils"
)

func main() {
	vp := viper.New()
	vp.SetConfigName("test")
	vp.SetConfigType("json")
	vp.AddConfigPath("./viper")
	err := vp.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(vp.GetString("test"))
	vp.Set("name", "estevão")
	vp.WriteConfig()

	vp.OnConfigChange(func(in fsnotify.Event) {
		fmt.Printf("file changed: %s\n\n", in.Name)
	})
	vp.WatchConfig()
	/*for {
	}*/

	config, err := utils.LoadConfig()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(config)
	}

}
