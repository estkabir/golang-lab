package utils

import "github.com/spf13/viper"

type DbConfig struct {
	Address  string `map structure:"address"`
	Port     string `map structure:"port"`
	Password string `map structure:"password"`
	User     string `map structure:"user"`
}

type ServerConfig struct {
	Port string `map structure:"port"`
}

type Config struct {
	Db     DbConfig     `map structure:"db"`
	Server ServerConfig `map structure:"server"`
}

var vp *viper.Viper

func LoadConfig() (Config, error) {
	vp = viper.New()
	var config Config

	vp.SetConfigName("config")
	vp.SetConfigType("json")
	vp.AddConfigPath("./viper/utils")
	vp.AddConfigPath(".")
	err := vp.ReadInConfig()
	if err != nil {
		return Config{}, err
	}
	err = vp.Unmarshal(&config)
	if err != nil {
		return Config{}, err
	}

	return config, err
}
