package main

import (
	"encoding/json"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"image/color"
	"net/http"
	"os"
	"time"
)

var client *http.Client

type randomFact struct {
	Text string `json:"text"`
}

func getRandomFact() (randomFact, error) {
	var fact randomFact

	response, err := client.Get("https://uselessfacts.jsph.pl/api/v2/facts/random")
	if err != nil {
		return randomFact{}, err
	}
	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&fact)
	if err != nil {
		return randomFact{}, err
	}

	return fact, nil
}

func main() {
	client = &http.Client{Timeout: 10 * time.Second}
	os.Setenv("FYNE_THEME", "light")
	a := app.New()
	window := a.NewWindow("Get useless fact")
	window.Resize(fyne.NewSize(800, 300))

	title := canvas.NewText("get Your useless fact", color.Black)
	title.TextStyle = fyne.TextStyle{
		Bold: true,
	}
	title.Alignment = fyne.TextAlignCenter
	title.TextSize = 24

	factText := widget.NewLabel("")
	factText.Wrapping = fyne.TextWrapBreak

	button := widget.NewButton("Get fact", func() {
		factText.SetText("LOADING...")

		fact, err := getRandomFact()
		if err != nil {
			dialog.ShowError(err, window)
			factText.SetText("")
		} else {
			factText.SetText(fact.Text)
		}
	})

	hbox := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), button, layout.NewSpacer())
	vbox := container.New(layout.NewVBoxLayout(), title, hbox, widget.NewSeparator(), factText)
	window.SetContent(vbox)
	window.ShowAndRun()
}
