package main

import "fmt"

func minInt(valOne int, valTwo int) int {
	if valOne < valTwo {
		return valOne
	}
	return valTwo
}

type minTypes interface {
	~float64 | int
}

func max[T minTypes](valOne T, valTwo T) T {
	if valOne > valTwo {
		return valOne
	}
	return valTwo
}

func min[T minTypes](valOne T, valTwo T) T {
	if valOne < valTwo {
		return valOne
	}
	return valTwo
}

func median[T minTypes](valOne T, valTwo T) T {
	return (valOne + valTwo) / 2
}

func main() {
	// fmt.Println(min(1, 2))
	fmt.Println(max(0.5, 0.7))
	fmt.Println(min(0.5, 0.3))
	fmt.Println(median(1.0, 2.3))
}
