package main

import (
	"fmt"
)

func Add[T float64 | int | float32](a T, b T) T {
	return a + b
}

func MapValues[T any](m map[string]T, f func(T) T) map[string]T {
	for k, v := range m {
		m[k] = f(v)
	}
	return m
}

type CustomData interface {
	int | float64 | float32 | []byte | []rune
}

type User[T CustomData] struct {
	ID   int
	Name string
	Data T
}

type CustomMap[T comparable, V int | string] map[T]V

func main() {
	//fmt.Println(Add(1, 2))
	//fmt.Println(Add(1.3, 2.2))
	result := MapValues(map[string]int{"a": 1, "b": 2}, func(x int) int {
		return x * 2
	})
	fmt.Println(result)

	user := &User[int]{ID: 1, Name: "Ryan", Data: 100}
	fmt.Println(user)

	m := make(CustomMap[int, string])
	m[3] = "3"
}
