package main

import (
	"fmt"
	"net/http"
)

func returnStringSum(val1 int, val2 *int) string {
	return fmt.Sprintf("%d", val1+*val2)
}

func sum(nums ...int) int {
	sum := 0
	for num := range nums {
		sum += +num
	}
	return sum
}

func factorial(n int) int {
	if n == 1 {
		return 1
	}
	return n + factorial(n-1)
}

func anonymous() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("reached")
	})
	f := func() {
		fmt.Println("anonymous function")
	}
	f()
}

func counter() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func fibonacci() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return b - a
	}
}

func main() {
	/*val2 := 100
	result := returnStringSum(20, &val2)
	fmt.Println(result)
	fmt.Printf("%d\n", sum(1, 2, 3, 4, 5, 6))
	fmt.Printf("%d\n", factorial(100))
	anonymous()
	counterRun := counter()
	fmt.Println(counterRun()) //1
	fmt.Println(counterRun()) //2
	fmt.Println(counterRun()) //3
	counterRun2 := counter()
	fmt.Println(counterRun2()) //1
	fmt.Println(counterRun2()) //2*/

	fib := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(fib())
	}
}
