package main

import (
	"fmt"
	"sync"
	"time"
)

func countDown(n int) {
	for n >= 0 {
		fmt.Println(n)
		n--
		time.Sleep(time.Second / 2)
	}
}

func main() {
	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		countDown(5)
		waitGroup.Done()
	}()
	waitGroup.Add(1)
	go func() {
		countDown(10)
		waitGroup.Done()
	}()
	waitGroup.Wait()
}
