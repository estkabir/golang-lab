package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type Collection struct {
	Mutex sync.Mutex
	Data  map[string]string
}

func NewCollection() *Collection {
	return &Collection{
		Data: make(map[string]string),
	}
}

func (c *Collection) Has(key string) bool {
	if _, ok := c.Data[key]; ok {
		return true
	}
	return false
}

func (c *Collection) Add(key string, value string) {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()
	if c.Has(key) {
		return
	}
	c.Data[key] = value
}

func main() {
	startTime := time.Now()
	fmt.Println("starting long running operation")

	var collections = NewCollection()
	var numberOfWorkers = 100
	waitGroup := &sync.WaitGroup{}
	limiter := make(chan int, 50)
	for i := 0; i < numberOfWorkers; i++ {
		waitGroup.Add(1)
		var entry = i
		limiter <- 1
		go func() {
			collections.Add(strconv.Itoa(entry), string(entry))
			time.Sleep(time.Second * 1)
			<-limiter
			waitGroup.Done()
		}()
	}
	waitGroup.Wait()
	fmt.Println("collections: ", len(collections.Data))
	endTime := time.Now().Sub(startTime)
	fmt.Println("All long running operations finished. execution time: [" + endTime.String() + "]")
}
