package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type UniqueName string

func LongRunningOperation(uniqueName string) UniqueName {
	time.Sleep(time.Second * 1)
	//fmt.Println("Long Running Operation Finished " + uniqueName)
	return UniqueName(uniqueName)
}

func main() {
	startTime := time.Now()
	fmt.Println("starting long running operation")

	var numberOfWorkers = 10000
	var allResults = make([]UniqueName, numberOfWorkers)
	waitGroup := &sync.WaitGroup{}
	limiter := make(chan int, 1000)
	for i := 0; i < numberOfWorkers; i++ {
		waitGroup.Add(1)
		var entry = i
		limiter <- 1
		go func() {
			result := LongRunningOperation(strconv.Itoa(entry))
			unique, _ := strconv.Atoi(string(result))
			allResults[unique] = result
			<-limiter
			waitGroup.Done()
		}()
	}
	waitGroup.Wait()
	fmt.Println("allResults: ", len(allResults))
	endTime := time.Now().Sub(startTime)
	fmt.Println("All long running operations finished. execution time: [" + endTime.String() + "]")
}
