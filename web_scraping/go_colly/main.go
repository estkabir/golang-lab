package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

func main() {
	//http://quotes.toscrape.com/random
	c := colly.NewCollector(
		colly.AllowedDomains("quotes.toscrape.com"),
	)
	c.OnRequest(func(request *colly.Request) {
		request.Headers.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36")
		fmt.Println("visiting", request.URL)
	})
	c.OnResponse(func(response *colly.Response) {
		fmt.Println("Response Code", response.StatusCode)
	})
	c.OnError(func(response *colly.Response, err error) {
		fmt.Println("error", err.Error())
	})
	//c.OnHTML(".text", func(element *colly.HTMLElement) {
	//	fmt.Println("Quote", element.Text)
	//})
	//c.OnHTML(".author", func(element *colly.HTMLElement) {
	//	fmt.Println("Author", element.Text)
	//})
	//c.Visit("http://quotes.toscrape.com/random")
	c.OnHTML(".quote", func(element *colly.HTMLElement) {
		div := element.DOM
		quote := div.Find(".text").Text()
		author := div.Find(".author").Text()
		tags := div.Find(".tag").Map(func(i int, selection *goquery.Selection) string {
			if i == 0 {
				return selection.Text()
			}
			return "," + selection.Text()
		})
		fmt.Printf("Quote: %s\nAuthor: %s\nTags size: %s\n\n", quote, author, tags)
	})
	c.Visit("http://quotes.toscrape.com")
}
